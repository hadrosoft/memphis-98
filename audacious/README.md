# Memphis 98
### Windows 9x-like skin for Audacious media player

<img src="../screenshots/0077 - audacious.png">

This is a Winamp classic-style skin for Audacious, meant to match the Memphis 98 theme. To add it to audacious, simply copy the <code>Memphis 98</code> directory to either <code>~/.local/share/audacious/Skins/</code> (user only) or <code>/usr/share/audacious/Skins/</code> (system-wide), then select them from the <i>Skinned Interface</i> tab in Preferences. Alternatively drag the skin file directly into the list view of available skins.

(via https://wiki.archlinux.org/index.php/audacious)
