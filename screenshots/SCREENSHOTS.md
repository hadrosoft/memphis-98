# Memphis 98
### Theme for GTK2/GTK3/Metacity, designed to emulate the look and feel of Windows 9x, with accompanying icon theme
#### by Clarissa Toney &lt;<clarissa@hadrosaurus.net>&gt;

## Screenshots

<img src="extras/header_screenshot_theme.png" width="800"><br>

### LightDM login screen
<img src="0010 - login_screen_wrong_password.png" width="800"><br>
<img src="0020 - login_screen.png" width="800"><br>

### Desktop screenshots
<img src="0031 - clean_desktop_2.png" width="800"><br>
<img src="0040 - menu_calendar.png" width="800"><br>
<img src="0050 - audacious_codeblocks.png" width="800"><br>
<img src="0060 - lock_screen.png" width="800"><br>
<img src="0070 - caja.png" width="800"><br>
<img src="0075 - pluma.png" width="800"><br>
<img src="0077 - audacious.png"><br>

### Widget Factory
<img src="0080 - widget_factory.png" alt="Widget Factory (Classic Theme)"><br>
<img src="0080 - widget_factory_standard.png" width="224" alt="Widget Factory (&quot;Standard&quot; Theme)">&nbsp;
<img src="0080 - widget_factory_rose.png" width="224" alt="Widget Factory (&quot;Rose&quot; Theme)">&nbsp;
<img src="0080 - widget_factory_red_white_blue.png" width="224" alt="Widget Factory (&quot;Rose&quot; Theme)"><br>
<img src="0080 - widget_factory_desert.png" width="224" alt="Widget Factory (&quot;Rose&quot; Theme)"><br>

### Submitted user screenshots
<img src="1000 - neon_city.png" width="800"><br>
