# Memphis 98
### Theme for GTK2/GTK3/Metacity, designed to emulate the look and feel of Windows 9x
#### by Clarissa Toney &lt;<clarissa@hadrosaurus.net>&gt;

<img src="../screenshots/extras/header_screenshot_theme.png" width="800">

## Motivation
The goal of this theme isn't to blindly copy everything about Windows 9x, down to its 16-color desktop backgrounds and chunky icons and non-antialiased fonts, et cetera. There are plenty of themes and setups that do exactly that. Memphis 98 isn't a novelty theme (or at least, isn't intended to be).

Even after 20+ years, I still believe "Windows Classic" is one of the best GUIs ever created, precisely because it's so utiliatarian. The neutral gray and blue colors are easy on the eyes. The 3D-beveled widgets can be easily distinguished from one another*. Since it values function over form, it excels at getting out of one's way, not making itself noticed, and letting one get on with their work, especially when paired with modern fonts and the right icon set.

There are other "Windows Classic"-like GTK themes out there, but they don't quite get it right. Sizes vary wildly between widget types, leading to a jarring and inconsistent user experience. Some widgets, especially comboboxes, don't even really look much like they did in classic Windows. They're good enough for some novelty screenshots (and I'm not knocking that), but I wanted something more consistent&mdash;something that was as attractive and easy to use as it was nostalgic. Instead of imposing my design ideas on one of those themes, I decided to start from scratch and write my own.

Memphis 98 seeks to fit in and still be usable in a modern Linux environment. It's actually my daily driver theme, and has been for the entire two years I've been developing it. Whenever a choice needed to be made between copying Windows Classic 100% to the pixel and making a particular GUI element usable, usability was chosen every time. For example, consideration has not been given to how this theme actually looks with original Win9x fonts (Fixedsys, Microsoft Sans Serif, et al). It was designed with the Ubuntu font family in mind.

At any rate, I hope somebody else finds it as useful and fun to use as I do.
<p>
<p>
<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* I'm still not convinced of the merits of flat themes from a usability standpoint.</i>



## Current status and testing
Memphis 98, so far, has only been tested under the MATE desktop environment, as that's what I use every day. There are still a lot of bugs and TODOs that I want to get taken care of before I move on to other environments. If you want to help, that's perhaps a good place to start. And as always, many applications do their own weird, annoying things with GUI widgets, and require their own special theming&mdash;which is why GTK themes end up being thousands of lines of CSS instead of hundreds.

## Color schemes

Thare are now multiple color variations of the theme:

- `Memphis-98`, the original gray-and-blue "Windows Classic" scheme from Windows 95
- `Memphis-98-Standard`, the beige "Windows Standard" scheme that was introduced in Windows 2000
- Rose, Desert, and others from the original set of 9x color schemes *(eventually want to convert them all!)*
- Once those are done, maybe I'll even start inventing all new palettes! (Or if you want to... have at it!)

In any setup directions that specify the theme's directory as `Memphis-98`, substitute the appropriate directory name for the theme variation you are using.


## Override file
Certain applications impose their own styling rules on the user and have to be forced to play along. [Click here](../override/) for details.


## Recommended settings
### MATE appearance properties:
- **Application font:** Ubuntu Regular 9
- **Document font:** Ubuntu Regular 9
- **Desktop font:** Ubuntu Bold 9
- **Window title font:** Ubuntu Bold 9
- **Fixed width font:** Ubuntu Mono Regular 11

### MATE menu preferences:
- **[Main button]**
    - **Show button icon:** YES
    - **Button text:** "Menu&nbsp;&nbsp;" *(no quotes; two spaces at end for padding)*
    - **Button icon:** ![](Memphis-98/gtk-3.0/assets/tux_menu.png) [gtk-3.0/assets/tux_menu.png](Memphis-98/gtk-3.0/assets/tux_menu.png)
- **[Plugins]**
    - **Border width:** 0 pixels
- **[Theme]**
    - **Use custom colors:** NO
    - **Theme:** Memphis 98 / "Desktop theme"

### MATE Terminal:
- **Font:** Perfect DOS VGA 437 Win Regular 11
- **Allow bold text:** NO
- **Text color:** #C0C0C0
- **Bold color:** Same as text color
- **Background color:** #000000
- **Palette:** Tango
- **Custom default terminal size:** 96 columns x 40 rows

### MATE panel properties:
- **Orientation:** Bottom
- **Size:** 28 pixels
- **Expand:** YES

### MATE notification settings:
- **Theme:** Nodoka
- **Position:** Bottom Right

### LightDM GTK+ Greeter settings: *(see below for notes)*
- **[Appearance]**
    - **Theme:** Memphis 98
    - **Icons:** Memphis 98
    - **Font:** Ubuntu Regular 11
- **[Background]**
    - **Color:** #008080
    - **Use user wallpaper if available:** NO



## Special consideration: monospace fonts
There are a number of options for fixed-width fonts. I suppose for a classic, more "authentic" experience, one could use Fixedsys Excelsior at size 12. But, as stated above, this theme wasn't created so I could be able to pretend it's still 1995, but because I think it's still a darn good interface.

Personally, I use Ubuntu Mono (at size 11) because it's modern, it fits in well with the rest the Ubuntu ecosystem, and it just plain looks good. The exception? The terminal, where I use a VGA text mode-inspired font. That's my one concession to pure nostalgia. :-D  Of course, feel free to use whatever you want.



## LightDM greeter
This is not necessary, but on my system, I prefer to use the LightDM GTK+ Greeter instead of the more "modern" Slick Greeter. Instructions can be found elsewhere, but to make the switch (under Ubuntu), run the command:
```
sudo apt install lightdm-gtk-greeter lightdm-gtk-greeter-settings
```

Then, in the file `/etc/lightdm/lightdm.conf`, find the line that begins with: `greeter-session=` and change it to: `greeter-session=lightdm-gtk-greeter`.

Copy the Memphis 98 theme folder to `/usr/share/themes/`. LightDM may need to be granted permission to use the theme. To do this, run the command:
```
sudo chmod 555 -r /usr/share/themes/Memphis-98
```

*(A symlink may work instead of copying the entire directory; I've not tested that on my system yet.)*



## MATE System Monitor
This is subjective of course, but I think the MATE logo on the "System" tab in the System Monitor looks better in bright white instead of the default medium gray (at least, when using Memphis 98). If you wish to have a bright white version of it, run the following commands:
```
sudo mv /usr/share/pixmaps/mate-system-monitor/side.png /usr/share/pixmaps/mate-system-monitor/side.png.old
sudo cp ~/.themes/Memphis-98/gtk-3.0/assets/side.png /usr/share/pixmaps/mate-system-monitor/side.png
sudo chmod 644 /usr/share/pixmaps/mate-system-monitor/side.png
```

The commands assume you have the theme installed to `~/.themes/Memphis-98/`. Substitute the appropriate location if necessary.

Screenshots for reference if you want to decide first:<br>
<img src="../screenshots/extras/mate_system_monitor_logo_gray.png" width="320">
<img src="../screenshots/extras/mate_system_monitor_logo_white.png" width="320">



## About CSDs
Memphis 98 is intended to be used with the standard window titlebars and controls. It is recommended that you [install `gtk3-nocsd`](http://www.webupd8.org/2014/08/how-to-disable-gtk3-client-side.html) and run all applications with client-side decorations (CSD) disabled. No consideration has been given to making CSDs look good with this theme.

*(Personally, I'm not a fan of CSDs in general and think they look pretty ugly, even in themes that are designed with them in mind.)*

If a CSD-enabled application does happen to look good in Memphis 98, it's by accident&mdash;it certainly wasn't on purpose.



## Things that need testing:
- Desktop environments other than MATE
- Renegade apps that don't play by the rules
- HiDPI *(I don't have a 4K monitor)*
