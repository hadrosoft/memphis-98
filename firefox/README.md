# Memphis 98
### Theme modifications for Firefox

To install: copy <code>userChrome.css</code> into your Firefox profile directory, which is usually located in <code>~/.mozilla/firefox/</code>.

This stylesheet is far from complete; feel free to contribute.
