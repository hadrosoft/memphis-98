# Memphis 98
### CSS patch file

Several GTK applications use their own CSS to theme various UI elements. In a lot of cases, it doesn't work well and clashes with the user theme.

To override this, copy `gtk.css` into `/home/$USER/.config/gtk-3.0/`.
```
cp override/gtk.css ~/.config/gtk-3.0/
```
*(You may have to create the "gtk-3.0" directory if it is not there.)*
```
mkdir -p ~/.config/gtk-3.0
```

**Warning:** If you ever want to change your system theme to anything else, don't forget to remove the `gtk.css` override file! It makes adjustments based on this theme which might break other themes.

Log out then log back after installing, or re-start any affected applications.

* * *

Examples of styling inconsistencies that this file addresses:
- [#1: mate-control-center not theming correctly](#1)
- [#2: gnome-disks not theming correctly](#2)
- [#3: MATE Window List applet (wnck-applet) button spacing](#3)
