# Memphis 98
# Optional bits for `.bashrc`

Below are a some snippets of code I use in my personal `.bashrc` file that you might find useful. If you don't use the terminal, then these aren't important. If you want to, just copy and paste these into the appropriate locations of your configuration file, located at `~/.bashrc`.

## Terminal prompt
<pre>
unset color_prompt force_color_prompt
export PS1="\n\[$(tput sgr0)\]\[\033[38;5;11m\]\u@\[$(tput sgr0)\]\[\033[38;5;15m\]\[$(tput sgr0)\]\[\033[38;5;11m\]\H:\[$(tput sgr0)\]\[\033[38;5;7m\]\n\w> \[$(tput sgr0)\]"
</pre>

This will set the prompt to print your `username@machine` in yellow (or whatever the equivalent slot in your terminal color scheme of choice is), and print the current directory, like so:

<img src="../screenshots/extras/terminal_prompt.png">

## System update

A little function to update all the installed software on the system, through both apt and snap.

<pre>
update()
{
    sudo echo -n ""
    echo -e "\e[38;5;00;48;5;3m╔═══════════════════════════════════╗\e[0m"
    echo -e "\e[38;5;00;48;5;3m║   Starting full system update...  ║\e[0m"
    echo -e "\e[38;5;00;48;5;3m╚═══════════════════════════════════╝\e[0m"
    echo
    echo -e "\e[38;5;15;48;5;6m┌───────────────────────────────────┐\e[0m"
    echo -e "\e[38;5;15;48;5;6m│      Updating repositories...     │\e[0m"
    echo -e "\e[38;5;15;48;5;6m└───────────────────────────────────┘\e[0m"
    sudo apt update
    echo
    echo -e "\e[38;5;15;48;5;6m┌───────────────────────────────────┐\e[0m"
    echo -e "\e[38;5;15;48;5;6m│      Upgrading all packages...    │\e[0m"
    echo -e "\e[38;5;15;48;5;6m└───────────────────────────────────┘\e[0m"
    sudo apt dist-upgrade -yy
    echo
    echo -e "\e[38;5;15;48;5;6m┌───────────────────────────────────┐\e[0m"
    echo -e "\e[38;5;15;48;5;6m│ Removing unneeded dependencies... │\e[0m"
    echo -e "\e[38;5;15;48;5;6m└───────────────────────────────────┘\e[0m"
    sudo apt autoclean
    sudo apt autoremove -yy
    echo
    echo -e "\e[38;5;15;48;5;6m┌───────────────────────────────────┐\e[0m"
    echo -e "\e[38;5;15;48;5;6m│         Updating snaps...         │\e[0m"
    echo -e "\e[38;5;15;48;5;6m└───────────────────────────────────┘\e[0m"
    sudo snap refresh
    echo
    echo -e "\e[93mUpdate complete!\e[0m"
    return
}

export -f update
</pre>

## Welcome message

<img src="../screenshots/extras/terminal_welcome.png">

```
display_center()
{
    columns="$(tput cols)"
    while IFS= read -r line; do
        printf "%*s\n" $(( (${#line} + columns) / 2)) "$line"
    done < "$1"
}

export -f display_center

. /etc/os-release
neofetch --ascii_distro ubuntu-mate
display_center <(echo "$(lsb_release -i -s) $XDG_CURRENT_DESKTOP $VERSION")
display_center <(echo "$(uname -s -r -p)")
display_center <(echo -n "System up since $(uptime -s)"
                 echo    "  ($(uptime -p | sed 's/\ weeks/w/g' | sed 's/\ week/w/g' | sed 's/\ days/d/g' | sed 's/\ day/d/g' |\
                          sed 's/\ hours/h/g' | sed 's/\ hour/h/g' | sed 's/\ minutes/m/g' | sed 's/\ minute/m/g' | sed 's/up //g' |\
                          sed 's/,//g'))")
echo
display_center <(echo "-- Welcome, $(getent passwd `whoami` | cut -d ':' -f 5 | cut -d ',' -f 1)! --")
```

This displays system information and a little welcome message when you first open up a terminal. :) You'll need to install `neofetch`. You can substitute the `--ascii_distro ubuntu-mate` part with whatever distro you like (see neofetch's documentation), or just omit it altogether for the default distro art for your distro. I specify because on Ubuntu MATE, neofetch defaults to the stock Ubuntu logo.

[Click here](https://github.com/dylanaraps/neofetch/wiki/Customizing-Info) for info about customizing neofetch's output.
