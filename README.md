# Memphis 98
### Theme for GTK2/GTK3/Metacity, designed to emulate the look and feel of Windows 9x, with accompanying icon theme
#### by Clarissa Toney &lt;<clarissa@hadrosaurus.net>&gt;

<img src="screenshots/extras/header_screenshot_theme.png" width="800">

Accepting contributions! See various READMEs for details.

## [System theme](theme/)
## [Icon theme](icons/)
## [Firefox userChrome](firefox/)
## [Audacious skin](audacious/)
## [Wallpaper](extras/wallpaper)
## [Terminal options](extras/TERMINAL_OPTIONS.md)
## [Screenshots](screenshots/SCREENSHOTS.md)
