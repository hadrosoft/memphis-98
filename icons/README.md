# Memphis 98
### MATE/GNOME icon theme intended to go with the GUI theme of the same name
#### by Clarissa Toney &lt;<clarissa@hadrosaurus.net>&gt;

<img src="../screenshots/extras/header_screenshot_icons.png" width="800">

This icon theme, which uses [Classic95](https://github.com/quadpixels/classic95) as its base, is an attempt to re-imagine the Windows 95 icon theme as a modern icon set. The goal is to retain the same simple, striking, colorful art style, but with the added benefit of full color and high resolution.

## Directories

There are two directories here: "Memphis-98" and "working."

- ["Memphis-98"](Memphis-98/) is the downloadable and ready-to-use icon set. It contains the current set of re-drawn icons, along with any icons from Classic95 that have not been redone yet.<p>This directory is the equivalent of <code>~/.icons/Memphis-98</code> on a local system. Download that directory as an archive in order to install the theme on your system.
- ["working"](working/) is the directory that I'm using to develop the icon set. It's a mirror of my working directory on my local machine. The structure of that directory and a description of my current workflow is in ["working/README.md"](working/README.md)</a>.



## One little problem
I really need a lot of help with these icons. One major stumbling block is that I have no experience or skill with vector drawing. All the icons completed to date have been done as raster graphics (PNG instead of SVG). So far it's working out&mdash;the few I've managed to do this way look pretty good&mdash;but it's a lot of work, and I'm not sure how futureproof it is. (Plus, there's a ton of icons, and it's going to take me forever to get through them all!) If anybody has experience making icons and wants to council me on this, or even wants to take on the project themselves, feel free to let me know!



## Guidelines

1. Don't feel beholden to the original icons' colors! They were limited to the 16-color EGA palette&mdash;we aren't. The goal of this icon set is to retain the art style but not the technical limitations. Some examples of embellishments I've already made:
    - The "Computer" icon has been changed from light gray to light beige
    - The "Desktop" icon has been changed from a plain gray slab to a brown wood table top
    - The "Folder" and "Recycle Bin" (Trash) icons have had subtle gradients added for depth
Of course, artist's discretion is always at play. For example, on the Desktop, I stayed with the bright teal color for the lamp simply because I like that color.
2. Feel free to add detail, as long as the final result still fits in with the art style. For example, the Desktop now has a glow emanating from the lamp. The computer has a detailed keyboard instead of a simple grid pattern, as well as increased detail and color depth on the monitor screen.
3. Icons should be drawn with bold outlines, simulating the crisp line-art look of the original icon set, but in high resolution. Corners should usually be rounded instead of perfect right angles (unless a particular icon calls for it). See [devices/256/computer.png](Memphis-98/devices/256/computer.png) for a particularly good example of this&mdash;the final effect is as if a lower-resolution raster drawing has been run through the Trace Pixel Art tool in Inkscape, and then cleaned up.
4. Don't specify app-specific icons. For example, don't replace the Firefox icon with the Netscape icon just because "lol 1995." Best to not specify an icon at all, and let the app itself supply its own. An exception is the desktop environment's default apps (text editor, calculator, etc); those applications straddle the line between standalone app and OS utility and are expected to blend in and not draw attention to themselves.



## Drawing process
<i>(Note that this describes my current process of re-drawing icons as raster images. If this set is converted to vector images, then an entirely different process will probably apply.)</i>

The original Windows 95 icon set was drawn at several sizes, with differing levels of detail. For that reason, each of Memphis 98's redraws are also based on several different sizes. Typically, the way I approach an icon is this:</p>

- For sizes 16x16, 22x22, and 24x24, I simply re-color the existing Win95 icon.
- For sizes 32x32* and above, I re-draw the icon in ultra high resolution, with the original icon magnified on a background layer so I can use it as a base.
    - For 32x32 and 64x64, I draw at 640x640.<br>
      <i>* (32x32 is a special case&mdash;sometimes the original icon looks great recolored; other times it needs to re-drawn. This is decided on a case-by-case basis.)</i>
    - For 48x48, 96x96, 128x128, and 256x256, I draw at 960x960.
- The thickness of the outlines of the icon's component shapes are drawn in multiples of 10 pixels. (Typically for the 960x960 base drawing, it's 10 pixels and 20 pixels for 640x640.) The outlines should also be aligned to a grid of that same size, so they create clean pixel lines when scaled down.
    - I'm not sure if it makes any difference, but I avoid anti-aliasing when drawing the outlines, as those will be added when the image is downsampled anyway.
- When a re-draw is complete, it is then scaled down to the sizes listed above. Scaling should be done with bilinear sampling; my experience has been that bicubic sampling is too soft and blurry, and that "bicubic sharper" creates unsightly halos.
    - Note that even after scaling, some touch-up work on the pixel level may still be required.

Yes, it's a lot of work. Yes, I'm nuts for even attempting to do it this way. :-) Again, if anybody wants to help, have at it!
