#ifndef CLARISSA_ICON_COPY_SRC_FILE_META_H_INCLUDED
#define CLARISSA_ICON_COPY_SRC_FILE_META_H_INCLUDED

/*
========================================
File_Access
========================================
*/

enum class File_Access
{
    Input,
    Output,
    Append,
    Random,
    Random_Append
};

/*
========================================
File_Data
========================================
*/

enum class File_Data
{
    Ascii,
    Binary
};

/*
========================================
File_Pos_Relative
========================================
*/

enum class File_Pos_Relative
{
    Beginning,
    Current,
    Eof
};

#endif      //  CLARISSA_ICON_COPY_SRC_FILE_META_H_INCLUDED
