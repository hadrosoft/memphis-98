#include "file_system.h"

#include <cstdio>
#include <limits>
#include <algorithm>
#include <fstream>
#include <cstring>
#include <experimental/filesystem>

#include <limits.h>
#include <libgen.h>
#include <unistd.h>

#include "common.h"

namespace fs = std::experimental::filesystem::v1;

/*
========================================
file_exists()
========================================
*/

bool file_exists (const std::string& filename)
{
    return fs::exists(filename);
}

/*
========================================
directory_exists()
========================================
*/

bool directory_exists (
    const std::string&  directory_name,
    const bool          make_on_fail    /*= false*/)
{
    if (fs::exists(directory_name)) {
        if (!fs::is_directory(directory_name)) {
            throw "error: " + directory_name + " is not a directory";
        }
        return true;
    }
    else {
        return (make_on_fail && fs::create_directory(directory_name));
    }
}

/*
========================================
get_directory()
========================================
*/

std::string get_directory (const std::string& path_name)
{
    char last_char = path_name.back();

    //  If the path is a directory, we're done
    if (last_char == '\\' || last_char == '/') { return path_name; }

    fs::path path(path_name);
    if (fs::is_directory(path) || !path.has_filename()) { return path_name; }
    return path.remove_filename().string();
}

/*
========================================
extract_filename()
========================================
*/

std::string extract_filename (const std::string& path_name)
{
    if (path_name.back() == '\\' || path_name.back() == '/') { return ""; }

    for (size_t index = path_name.size(); index --> 0;) {
        if (path_name[index] == '\\' || path_name[index] == '/') {
            return path_name.substr(index + 1);
        }
    }

    return path_name;
}

/*
========================================
strip_file_extension()
========================================
*/

std::string strip_file_extension (const std::string& filename)
{
    if (filename.empty()) { return filename; }

    for (size_t index = filename.size(); index --> 0;) {
        if (filename[index] == '.' && index != 0) {
            return filename.substr(0, index);
        }
    }

    return filename;
}

/*
========================================
kill_file()
========================================
*/

bool kill_file (
    const std::string&  filename,
    const bool          backup_first    /*= false*/)
{
    if (backup_first) {
        if (!file_exists(filename + ".bak")) {
            return (std::rename(filename.c_str(), (filename + ".bak").c_str()) == 0);
        }
        else {
            size_t attempt = 0;
            while (true) {
                std::string target_name = filename + ".bak" + numstr(attempt);
                if (file_exists(target_name)) {
                    ++attempt;
                    continue;
                }
                else {
                    return (std::rename(filename.c_str(), target_name.c_str()) == 0);
                }
            }
        }
        return false;
    }

    return (std::remove(filename.c_str()) == 0);
}

/*
========================================
copy_file()
========================================
*/

bool copy_file (
    const std::string&  source_filename,
    const std::string&  new_filename,
    const bool          overwrite_if_exists)
{
    if (!file_exists(source_filename)) { return false; }
    if (file_exists(new_filename) && !overwrite_if_exists) { return false; }

    kill_file(new_filename);

    fs::copy(source_filename, new_filename);
    return file_exists(new_filename);
}

/*
========================================
rename_file()
========================================
*/

bool rename_file (
    const std::string&  old_filename,
    const std::string&  new_filename)
{
    return (std::rename(old_filename.c_str(), new_filename.c_str()) == 0);
}

/*
========================================
file_size()
========================================
*/

size_t file_size (const std::string& filename)
{
    if (fs::is_regular_file(filename)) {
        return fs::file_size(filename);
    }
    else {
        throw "error: '" + filename + "' is not a regular file";
    }
}

/*
========================================
file_type()
========================================
*/

File_Type file_type (const std::string& filename)
{
    return  (!fs::exists(filename)) ? File_Type::Does_Not_Exist :
            (fs::is_directory(filename)) ? File_Type::Directory : File_Type::File;
}

/*
========================================
simplified_path()
========================================
*/

std::string simplified_path (std::string path_name)
{
    std::replace(path_name.begin(), path_name.end(), '\\', '/');

    std::vector<std::string> ps = split_string(path_name, '/');
    std::string p               = "";
    std::vector<std::string> st;
    for (size_t index = 0; index < ps.size(); ++index) {
        if (ps[index] =="..") {
            if (!st.empty()) { st.pop_back(); }
        }
        else if (ps[index] != ".") {
            st.push_back(ps[index]);
        }
    }
    for (size_t index = 0; index < st.size(); ++index) {
        p += "/" + st[index];
    }
    return (p.empty()) ? "/" : p;
}

/*
========================================
file_is_safe_for_writing()
========================================
*/

bool file_is_safe_for_writing (const std::string& filename)
{
    const std::ofstream ofs(filename);
    return ofs.is_open();
}


/*
========================================
filename_match()
========================================
*/

bool filename_match (std::string mask, std::string filename)
{
    //  Written by Jack Handy -- jakkhandy@hotmail.com
    //  http://www.codeproject.com/Articles/1088/Wildcard-string-compare-globbing

    //  Note: we don't use it here, but if you need case-insensitive matching,
    //  wrap any character comparisons in lcase() or ucase() calls.

    if (mask.empty() || filename.empty()) {
        throw "error: either mask or filename are empty";
    }

    mask     = simplified_path(mask);
    filename = simplified_path(filename);

    const char* cp      = nullptr;
    const char* mp      = nullptr;
    const char* wild    = mask.c_str();
    const char* str     = filename.c_str();

    while ((*str) && (*wild != '*')) {
        if ((*wild != *str) && (*wild != '?')) { return 0; }
        ++wild;
        ++str;
    }

    while (*str) {
        if (*wild == '*') {
            if (!*++wild) { return 1; }
            mp = wild;
            cp = str + 1;
        }
        else if ((*wild == *str) || (*wild == '?')) {
            wild++;
            str++;
        }
        else {
            wild = mp;
            str  = cp++;
        }
    }

    while (*wild == '*') { wild++; }
    return !*wild;
}

/*
========================================
get_exepath()
========================================
*/

std::string get_exepath()
{
    char          buff   [PATH_MAX];
    const ssize_t len  = ::readlink("/proc/self/exe", buff, sizeof(buff) - 1);
    if (len != -1) {
        buff[len] = '\0';
        return std::string(buff);
    }
    return "";
}

/*
========================================
get_exedir()
========================================
*/

std::string get_exedir()
{
    std::string exestr = get_exepath();
    char* const exe    = new char[exestr.size() + 1];
    std::strcpy(exe, exestr.c_str());
    return std::string(dirname(exe));
}
