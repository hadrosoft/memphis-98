#include "iofile.h"

#include "file_system.h"
#include "string.h"

/*
========================================
iofile::iofile()
========================================
*/

iofile::iofile (
    const std::string& filename_,
    const File_Access  amode,
    const File_Data    dmode) :
        _filename       (simplified_path(filename_)),
        _access_mode    (amode),
        _data_mode      (dmode)
    {
        open(_access_mode, _data_mode);
    }

/*
========================================
iofile::~iofile()
========================================
*/

iofile::~iofile()
{
    close();
}

/*
========================================
iofile::open()
========================================
*/

void iofile::open (const File_Access amode, const File_Data dmode) const
{
    std::ios_base::openmode mode;

    if (amode == File_Access::Input) {
        mode = std::ios_base::in;
    }
    else if (amode == File_Access::Output) {
        mode = std::ios_base::out;
    }
    else if (amode == File_Access::Random ||
             amode == File_Access::Random_Append)
    {
        mode = std::ios_base::out | std::ios_base::in;
    }

    if (amode == File_Access::Append || amode == File_Access::Random_Append) {
        mode |= std::ios_base::app;
    }

    if (dmode == File_Data::Binary || amode == File_Access::Input) {
        mode |= std::ios_base::binary;
    }

    _file.open(_filename, mode);
    _access_mode = amode;
    _data_mode   = dmode;
}

/*
========================================
iofile::close()
========================================
*/

void iofile::close() const
{
    _file.close();
}

/*
========================================
iofile::filename()
========================================
*/

const std::string& iofile::filename() const
{
    return _filename;
}

/*
========================================
iofile::file_size()
========================================
*/

size_t iofile::file_size() const
{
    return ::file_size(_filename);
}

/*
========================================
iofile::current_pos()
========================================
*/

size_t iofile::current_pos() const
{
    const auto ret = _file.tellg();
    if (ret == std::fstream::pos_type(-1)) { return file_size(); }
    return ret;
}

/*
========================================
iofile::seek()
========================================
*/

void iofile::seek (
    const size_t            offset,
    const File_Pos_Relative origin  /*= File_Pos_Relative::Beginning*/) const
{
    const auto seek_origin =
        (origin == File_Pos_Relative::Beginning) ? std::ios_base::beg :
        (origin == File_Pos_Relative::Current)   ? std::ios_base::cur :
        (origin == File_Pos_Relative::Eof)       ? std::ios_base::end : std::ios_base::end;
    _file.seekg(offset, seek_origin);
    _file.seekp(offset, seek_origin);
}

/*
========================================
iofile::eof()
========================================
*/

bool iofile::eof() const
{
    return (_file.eof() || current_pos() >= file_size());
}

/*
========================================
iofile::set_mode()
========================================
*/

void iofile::set_mode (
    File_Access     new_access_mode,
    const File_Data new_data_mode) const
{
    if (new_access_mode == _access_mode &&
        new_data_mode == _data_mode)
    {
        return;
    }

    if (new_access_mode == File_Access::Output ||
        new_access_mode == File_Access::Random_Append)
    {
        new_access_mode = File_Access::Append;
    }

    const size_t cur_pos = current_pos();
    close();
    open (new_access_mode, new_data_mode);
    seek (cur_pos);
}


void iofile::set_mode (const File_Access new_access_mode) const
{
    set_mode(new_access_mode, _data_mode);
}


void iofile::set_mode (const File_Data new_data_mode) const
{
    set_mode(_access_mode, new_data_mode);
}

/*
========================================
iofile::line_input()
========================================
*/

std::string iofile::line_input() const
{
    std::string val;
    line_input(val);
    return val;
}


void iofile::line_input (std::string& target_string) const
{
    target_string.clear();
    std::istream::sentry  se   (_file, true);
    std::streambuf* const sb = _file.rdbuf();

    for (;;) {
        const int c = sb->sbumpc();
        if (c == '\n') {
            return;
        }
        else if (c == '\r') {
            if (sb->sgetc() == '\n') { sb->sbumpc(); }
            return;
        }
        else if (c == EOF) {
            if (target_string.empty()) {
                _file.setstate(std::ios::eofbit);
            }
            return;
        }
        else {
            target_string += static_cast<char>(c);
        }
    }
}

/*
========================================
iofile::put()
========================================
*/

void iofile::put (
    const void* const   buffer,
    const size_t        size,
    const size_t        count) const
{
    _file.write((std::fstream::char_type*)buffer, size * count);
}


void iofile::put (const void* const buffer, const size_t byte_count) const
{
    put(buffer, byte_count, 1);
}

/*
========================================
iofile::write_string()
========================================
*/

void iofile::write_string (const std::string& str) const
{
    if (_data_mode == File_Data::Ascii) {
        write_ascii_string(str);
        return;
    }

    write_size_t(str.size());
    if (!str.empty()) { put(&str.front(), str.size()); }
}

/*
========================================
iofile::write_naked_string()
========================================
*/

void iofile::write_naked_string (const std::string& str) const
{
    /*  Writes a naked (untagged) string to file. Normal strings are Pascal-
        style, with an int written first to indicate the string's length.   */

    if (_data_mode == File_Data::Ascii) {
        write_ascii_string(str);
        return;
    }

    if (!str.empty()) { put(&str.front(), str.size()); }
}

/*
========================================
iofile::write_ascii_string()
========================================
*/

void iofile::write_ascii_string (const std::string& str) const
{
    if (!str.empty()) { put(&str.front(), str.size()); }
    static const std::string newline = "\n";
    put(&newline.front(), 1);
}

/*
========================================
iofile::write_size_t()
========================================
*/

void iofile::write_size_t (const size_t value) const
{
    put(&value, sizeof(size_t));
}
