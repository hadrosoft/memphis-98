#ifndef CLARISSA_ICON_COPY_SRC_COMMON_H_INCLUDED
#define CLARISSA_ICON_COPY_SRC_COMMON_H_INCLUDED

#include <string>
#include <limits>
#include <sstream>
#include <vector>

bool                     is_number            (const std::string& str);
double                   strval               (const std::string& str);
bool                     range                (float value, float min, float max);
std::string              intstr               (int val);
std::string              numstr_separators    (float val);
std::string              intstr_separators    (int val);
float                    frac                 (float value);
std::string              safe_substr          (const std::string& str, size_t pos = 0, size_t n = std::string::npos);
std::string              safe_substr_range    (const std::string& str, size_t first_char_pos, size_t last_char_pos);
std::string              safe_substr_to_char  (const std::string& str, size_t first_char_pos, char find_char);
std::string              safe_substr_to_chars (const std::string& str, size_t first_char_pos, const std::string& find_any_char);
std::vector<std::string> split_string         (const std::string& str, char delimiter);
void                     string_replace       (std::string& str, const std::string& check_str, const std::string& replace_str);
std::string              string_replace       (const std::string& str, const std::string& check_str, const std::string& replace_str);
void                     string_replace       (std::string& str, size_t start_pos, size_t end_pos, const std::string& replace_str);
std::string              string_replace       (const std::string& str, size_t start_pos, size_t end_pos, const std::string& replace_str);
std::string              string_word          (const std::string& str, size_t word_number);
size_t                   string_word_char_pos (const std::string& str, size_t word_number);
std::string              ucase                (const std::string& str);
std::string              lcase                (const std::string& str);
std::string              ltrim                (const std::string& str, const std::string& trim_chars = " \t");
std::string              rtrim                (const std::string& str, const std::string& trim_chars = " \t");
std::string              trim                 (const std::string& str, const std::string& trim_chars = " \t");


/*
========================================
numstr()
========================================
*/

template <typename T> std::string numstr (
    const T         val,
    const size_t    decimal_places = std::numeric_limits<size_t>::max())
{
    #define SIZE_T_MAX std::numeric_limits<size_t>::max()

    static_assert(std::is_arithmetic<T>::value ||
                  std::is_enum<T>::value,
                  "argument to 'numstr' must be numeric value");

    std::stringstream stream;
    if (decimal_places != SIZE_T_MAX) {
        stream.precision(decimal_places + intstr(val).size());
    }

    if (std::is_same<T, bool>::value) {
        stream << std::boolalpha << val;
    }
    else {
        stream << val;
    }

    return stream.str();
}


template <typename T> std::string numstr (T* const val)
{
    std::stringstream stream;
    stream << val;
    return stream.str();
}

/*
========================================
clamp()
========================================
*/

template <typename T> void clamp (T& value, float low, float high)
{
    static_assert(std::is_arithmetic<T>::value,
                  "argument to 'clamp' must be arithmetic type (integral or "
                  "floating point)");
    if (low > high) { std::swap(low, high); }
    value = (value < low) ? low : (value > high) ? high : value;
}

/*
========================================
clamp_abs()
========================================
*/

template <typename T> void clamp_abs (T& value, const float limit)
{
    static_assert(std::is_arithmetic<T>::value,
                  "argument to 'clamp' must be arithmetic type (integral or "
                  "floating point)");

    if (std::abs(value) > std::abs(limit)) { value = std::abs(limit) * sgn(value); }
}

/*
========================================
clamped()
========================================
*/

template <typename T> T clamped (T value, const float low, const float high)
{
    clamp(value, low, high);
    return value;
}

/*
========================================
clamped_abs()
========================================
*/

template <typename T> T clamped_abs (T value, const float limit)
{
    clamp_abs(value, limit);
    return value;
}


#endif      //  CLARISSA_ICON_COPY_SRC_COMMON_H_INCLUDED
