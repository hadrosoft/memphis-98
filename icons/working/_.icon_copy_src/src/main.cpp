#include <iostream>

#include "common.h"
#include "file_system.h"
#include "iofile.h"

int copy_redraws();
int generate_index_file();

/*
========================================
main()
========================================
*/

int main (int, char**)
{
    int         option = 0;
    std::string input;
    int         ret    = 0;

    while (true)
    {
        std::cout << "[1] Copy icon redraws" << std::endl;
        std::cout << "[2] Generate icon index file" << std::endl;
        std::cout << "[3] Exit" << std::endl;
        std::cout << "   > ";
        std::cin >> input;

        if (!is_number(input) || !range(strval(input), 1, 3)) {
            std::cout << "Sorry, response must be a number [1-3]. Try again.";
            std::cout << std::endl << std::endl;
        }
        else {
            std::cout << std::endl << std::endl;
            option  = strval(input);

            if (option == 1) {
                ret = copy_redraws();
            }
            else if (option == 2) {
                ret = generate_index_file();
            }
            else if (option == 3) {
                break;
            }
        }
    }

    std::cout << std::endl << std::endl;
    std::cout << "Done! Press any key to continue" << std::endl;
    std::cin.get();

    return ret;
}

/*
========================================
copy_redraws()
========================================
*/

int copy_redraws()
{
    const std::string              current_directory = get_exedir();
    const std::string              redraw_directory  = current_directory + "/_.redraws";
    const std::string              info_file         = redraw_directory + "/redraw_targets.txt";
    std::string                    target_directory;
    const std::vector<std::string> size_names          { "16", "22", "24", "32", "48", "64", "96", "128", "256" };
    const std::vector<std::string> source_size_names   { "16", "22", "24", "32", "48", "scalable" };

    if (!file_exists(info_file)) {
        std::cerr << "error: file '" << info_file << "' does not exist" << std::endl;
        return 1;
    }

    struct Info_Target
    {
        std::string target_dir;
        std::string icon_name;
    };

    struct Info_IconSet
    {
        std::string              redraw_name;
        std::vector<Info_Target> target_names;
    };

    std::vector<Info_IconSet> redraws;

    const iofile file(info_file, File_Access::Input, File_Data::Ascii);

    while (!file.eof()) {
        const std::string line   = file.line_input();
        const std::string first  = string_word(line, 1);
        const std::string second = string_word(line, 2);

        if (lcase(first) == "target_directory") {
            target_directory = safe_substr(line, string_word_char_pos(line, 2));
        }
        else if (lcase(first) == "icon_name") {
            redraws.emplace_back();
            Info_IconSet& set = redraws.back();
            set.redraw_name   = second;

            while (!file.eof()) {
                const std::string icon_line   = file.line_input();
                const std::string icon_first  = string_word(icon_line, 1);
                const std::string icon_second = string_word(icon_line, 2);
                const std::string icon_third  = string_word(icon_line, 3);

                if (lcase(icon_first) == "target") {
                    set.target_names.emplace_back();
                    set.target_names.back().target_dir = icon_second;
                    set.target_names.back().icon_name  = icon_third;
                }
                else if (lcase(icon_first) == "end") {
                    break;
                }
            }
        }
    }

    //  Delete icon files in target directories (clean slate)
    const std::string delete_png = "find \"" + target_directory + "\" -type f -name '*.png' -exec rm {} +";
    const std::string delete_svg = "find \"" + target_directory + "\" -type f -name '*.svg' -exec rm {} +";
    std::system(delete_png.c_str());
    std::system(delete_svg.c_str());

    //  Copy redraws
    for (const Info_IconSet& set: redraws) {
        for (const std::string& size_tag: size_names) {
            const std::string file_to_copy = redraw_directory + "/" + set.redraw_name + "/" + set.redraw_name + "_" + size_tag + ".png";
            if (!file_exists(file_to_copy)) { continue; }
            for (const Info_Target& theme_name: set.target_names) {
                const std::string target_filename = target_directory + "/" + theme_name.target_dir + "/" + size_tag + "/" + theme_name.icon_name + ".png";
                copy_file(file_to_copy, target_filename, true);
            }
        }
    }

    //  Delete source theme files
    for (const Info_IconSet& set: redraws) {
        for (const std::string& size_tag: source_size_names) {
            for (const Info_Target& source_name: set.target_names) {
                const std::string look_directory = current_directory + "/" + source_name.target_dir + "/" + size_tag + "/";
                const std::string png_name       = look_directory + "/" + source_name.icon_name + ".png";
                const std::string svg_name       = look_directory + "/" + source_name.icon_name + ".svg";
                kill_file(png_name);
                kill_file(svg_name);
            }
        }
    }

    return 0;
}

/*
========================================
generate_index_file()
========================================
*/

int generate_index_file()
{
    //  Get target directory name
    const std::string              current_directory = get_exedir();
    const std::string              redraw_directory  = current_directory + "/_.redraws";
    const std::string              info_filename     = redraw_directory + "/redraw_targets.txt";
    std::string                    target_directory;

    if (!file_exists(info_filename)) {
        std::cerr << "error: file '" << info_filename << "' does not exist" << std::endl;
        return 1;
    }

    const iofile info_file(info_filename, File_Access::Input, File_Data::Ascii);

    while (!info_file.eof()) {
        const std::string line  = info_file.line_input();
        const std::string first = string_word(line, 1);

        if (lcase(first) == "target_directory") {
            target_directory = safe_substr(line, string_word_char_pos(line, 2));
        }
    }

    if (target_directory.empty()) {
        std::cerr << "error: no target directory specified!" << std::endl;
        return 1;
    }

    //  Build index file
    const std::vector<std::pair<std::string, std::string>> names
    {
        { "actions",        "Actions" },
        { "animations",     "Animations" },
        { "apps",           "Applications" },
        { "categories",     "Categories" },
        { "devices",        "Devices" },
        { "emblems",        "Emblems" },
        { "emotes",         "Emotes" },
        { "mimes",          "MimeTypes" },
        { "notifications",  "" },
        { "panel",          "" },
        { "places",         "Places" },
        { "status",         "Status" },
        { "stock",          "Stock" },
    };
    const std::vector<std::pair<std::string, std::string>> size_names
    {
        { "16",  "  1  16" },
        { "22",  " 17  22" },
        { "24",  " 23  24" },
        { "32",  " 25  32" },
        { "48",  " 33  48" },
        { "64",  " 49  64" },
        { "96",  " 65  96" },
        { "128", " 97 128" },
        { "256", "129 256" },
    };

    const std::string theme_name = "Memphis 98";
    const std::string comment    = "Remastered \"Classic\" icons for GNOME/MATE";
    const std::string inherits   = "Classic95";

    iofile file(target_directory + "/index.theme", File_Access::Output, File_Data::Ascii);

    file.write_string("[Icon Theme]");
    file.write_string("Name=" + theme_name);
    file.write_string("Comment=" + comment);
    if (!inherits.empty()) { file.write_string("Inherits=" + inherits); }
    file.write_string("Example=folder");

    std::string dir_str = "Directories=";
    for (const std::pair<std::string, std::string>& name_str: names) {
        for (const std::pair<std::string, std::string>& size_str: size_names) {
            dir_str += name_str.first + "/" + size_str.first + ",";
        }
    }
    if (!dir_str.empty() && dir_str.back() == ',') { dir_str.pop_back(); }
    file.write_string(dir_str);
    file.write_string("");


    for (const std::pair<std::string, std::string>& name_str: names) {
        if (name_str.second.empty()) { continue; }
        for (const std::pair<std::string, std::string>& size_str: size_names) {
            file.write_string("[" + name_str.first + "/" + size_str.first + "]");
            file.write_string("Size=" + size_str.first);
            file.write_string("MinSize=" + string_word(size_str.second, 1));
            file.write_string("MaxSize=" + string_word(size_str.second, 2));
            file.write_string("Context=" + name_str.second);
            file.write_string("Type=Scalable");
            file.write_string("");
        }
    }

    return 0;
}
