#ifndef CLARISSA_ICON_COPY_SRC_IOFILE_H_INCLUDED
#define CLARISSA_ICON_COPY_SRC_IOFILE_H_INCLUDED

#include <fstream>

#include "file_meta.h"

class iofile
{
    public:
        iofile  (const std::string& filename_, File_Access amode, File_Data dmode);
        ~iofile ();

        //  Management
        const std::string& filename    () const;
        size_t             file_size   () const;
        size_t             current_pos () const;
        void               seek        (size_t offset, File_Pos_Relative origin = File_Pos_Relative::Beginning) const;
        bool               eof         () const;
        void               set_mode    (File_Access new_access_mode, File_Data new_data_mode) const;
        void               set_mode    (File_Access new_access_mode) const;
        void               set_mode    (File_Data new_data_mode) const;

        //  Data output
        void               put                (const void* buffer, size_t size, size_t count) const;
        void               put                (const void* buffer, size_t byte_count) const;
        void               write_string       (const std::string& str) const;
        void               write_naked_string (const std::string& str) const;
        void               write_size_t       (size_t value) const;

        //  Data input
        std::string        read_string () const;
        void               read_string (std::string& target_string) const;
        std::string        read_string (size_t string_length) const;
        void               read_string (std::string& target_string, size_t string_length) const;
        std::string        line_input  () const;
        void               line_input  (std::string& target_string) const;

    private:
        void open (File_Access amode, File_Data dmode) const;
        void close() const;
        void write_ascii_string (const std::string& str) const;

        std::string          _filename;
        mutable std::fstream _file;
        mutable File_Access  _access_mode;
        mutable File_Data    _data_mode;
};

#endif      //  CLARISSA_ICON_COPY_SRC_IOFILE_H_INCLUDED
