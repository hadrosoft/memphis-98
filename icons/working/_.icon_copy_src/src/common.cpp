#include "common.h"

#include <cmath>

/*
========================================
is_number()
========================================
*/

bool is_number (const std::string& str)
{
    if (str.empty()) { return false; }
    char* end;
    std::strtod(str.c_str(), &end);
    return (*end == 0);
}

/*
========================================
strval()
========================================
*/

double strval (const std::string& str)
{
    return std::atof(str.c_str());
}

/*
========================================
range()
========================================
*/

bool range (const float value, float min, float max)
{
    if (min > max) { std::swap(min, max); }
    return (value >= min && value <= max);
}

/*
========================================
intstr()
========================================
*/

std::string intstr (const int val)
{
    return numstr(val);
}

/*
========================================
numstr_separators()
========================================
*/

std::string numstr_separators (const float val)
{
    if (std::fabs(val - static_cast<int>(val)) < 0.000001f) {
        return intstr_separators(val);
    }

    return intstr_separators(val) + safe_substr(numstr(frac(std::fabs(val))), 2);
}

/*
========================================
intstr_separators()
========================================
*/

std::string intstr_separators (const int val)
{
    std::string ret = intstr(val);
    int insert_position = ret.size() - 3;
    while (insert_position > 0) {
        ret.insert(insert_position, ",");
        insert_position -= 3;
    }

    return ret;
}

/*
========================================
frac()
========================================
*/

float frac (const float value)
{
    return value - static_cast<int>(value);
}

/*
========================================
safe_substr()
========================================
*/

std::string safe_substr (
    const std::string&  str,
    const size_t        pos /*= 0*/,
    const size_t        n   /*= std::string::npos*/)
{
    if (str.empty() || pos >= str.size()) { return ""; }
    return str.substr(pos, std::min(n, str.size() - pos));
}

/*
========================================
safe_substr_range()
========================================
*/

std::string safe_substr_range (
    const std::string&  str,
    size_t              first_char_pos,
    size_t              last_char_pos)
{
    if (str.empty()) { return str; }
    clamp(first_char_pos, 0, str.size() - 1);
    clamp(last_char_pos,  0, str.size() - 1);
    if (first_char_pos > last_char_pos) {
        std::swap(first_char_pos, last_char_pos);
    }
    return safe_substr(str, first_char_pos, last_char_pos - first_char_pos + 1);
}

/*
========================================
safe_substr_to_char()
========================================
*/

std::string safe_substr_to_char (
    const std::string&  str,
    const size_t        first_char_pos,
    const char          find_char)
{
    if (str.empty() || first_char_pos >= str.size()) { return ""; }
    for (size_t index = first_char_pos; index < str.size(); ++index) {
        if (str[index] == find_char) {
            return safe_substr_range(str, first_char_pos, index);
        }
    }
    return "";
}

/*
========================================
safe_substr_to_chars()
========================================
*/

std::string safe_substr_to_chars (
    const std::string&  str,
    const size_t        first_char_pos,
    const std::string&  find_any_char)
{
    if (str.empty() || first_char_pos >= str.size()) { return ""; }
    for (size_t index = first_char_pos; index < str.size(); ++index) {
        if (find_any_char.find(str[index]) != std::string::npos) {
            return safe_substr_range(str, first_char_pos, index);
        }
    }
    return "";
}

/*
========================================
split_string()
========================================
*/

std::vector<std::string> split_string (
    const std::string&  str,
    const char          delimiter)
{
    std::vector<std::string> ret;
    size_t j = 0;
    for (size_t index = 0; index < str.size(); ++index) {
        if (str[index] == delimiter &&
            !(index > 0 && str[index - 1] == '\\'))
        {
            const std::string cur = str.substr(j, index - j);
            if (!cur.empty()) { ret.push_back(cur); }
            j = index + 1;
        }
    }
    if (j < str.size()) { ret.push_back(str.substr(j)); }
    const std::string del_str   (1, delimiter);
    const std::string escape  = "\\" + del_str;
    for (std::string& check: ret) { string_replace(check, escape, del_str); }
    return ret;
}

/*
========================================
string_replace()
========================================
*/

void string_replace (
    std::string&        str,
    const std::string&  check_str,
    const std::string&  replace_str)
{
//    for (size_t pos = 0;; pos += replace_str.length()) {
//        pos = str.find(check_str, pos);
//        if (pos == std::string::npos) { break; }
//        str.erase  (pos, check_str.length());
//        str.insert (pos, replace_str);
//    }
    size_t n = 0;
    while ((n = str.find(check_str, n)) != std::string::npos) {
        str.replace(n, check_str.size(), replace_str);
        n += replace_str.size();
    }
}


std::string string_replace (
    const std::string&  str,
    const std::string&  check_str,
    const std::string&  replace_str)
{
    std::string ret = str;
    string_replace(ret, check_str, replace_str);
    return ret;
}


void string_replace (
    std::string&        str,
    size_t              start_pos,
    size_t              end_pos,
    const std::string&  replace_str)
{
    if (start_pos > end_pos) { std::swap(start_pos, end_pos); }
    str.replace(start_pos, (end_pos - start_pos + 1), replace_str);
}


std::string string_replace (
    const std::string&  str,
    const size_t        start_pos,
    const size_t        end_pos,
    const std::string&  replace_str)
{
    std::string ret = str;
    string_replace(ret, start_pos, end_pos, replace_str);
    return ret;
}

/*
========================================
string_word()
========================================
*/

std::string string_word (
    const std::string&  str,
    const size_t        word_number)
{
    std::stringstream ss(str);
    std::string       line;
    size_t            word_count = 0;

    while (std::getline(ss, line)) {
        size_t prev = 0;
        size_t pos;

        while ((pos = line.find_first_of(" \n\t\v\b\r\f\a\0", prev)) != std::string::npos) {
            if (pos > prev) {
                ++word_count;
                if (word_count == word_number) {
                    return trim(line.substr(prev, pos - prev));
                }
            }
            prev = pos + 1;
        }

        if (prev < line.length()) {
            ++word_count;
            if (word_count == word_number) {
                return trim(line.substr(prev, pos - prev));
            }
        }
    }
    return "";
}

/*
========================================
string_word_char_pos()
========================================
*/

size_t string_word_char_pos (
    const std::string&  str,
    const size_t        word_number)
{
    std::stringstream ss(str);
    std::string       line;
    size_t            word_count = 0;

    while (std::getline(ss, line)) {
        size_t prev = 0;
        size_t pos;

        while ((pos = line.find_first_of(" \n\t\v\b\r\f\a\0", prev)) != std::string::npos) {
            if (pos > prev) {
                ++word_count;
                if (word_count == word_number) { return prev; }
            }
            prev = pos + 1;
        }

        if (prev < line.length()) {
            ++word_count;
            if (word_count == word_number) { return prev; }
        }
    }
    return std::string::npos;
}

/*
========================================
ucase()
========================================
*/

std::string ucase (const std::string& str)
{
    std::string           new_string = str;
    std::string::iterator char_pos   = new_string.begin();
    std::string::iterator end        = new_string.end();

    while (char_pos != end) {
        *char_pos = std::toupper(static_cast<unsigned char>(*char_pos));
        ++char_pos;
    }
    return new_string;
}

/*
========================================
lcase()
========================================
*/

std::string lcase (const std::string& str)
{
    std::string           new_string = str;
    std::string::iterator char_pos   = new_string.begin();
    std::string::iterator end        = new_string.end();

    while (char_pos != end) {
        *char_pos = std::tolower((unsigned char)*char_pos);
        ++char_pos;
    }
    return new_string;
}

/*
========================================
ltrim()
========================================
*/

std::string ltrim (
    const std::string&  str,
    const std::string&  trim_chars  /*= " \t"*/)
{
    const auto begin = str.find_first_not_of(trim_chars);
    return (begin == std::string::npos) ? "" : safe_substr(str, begin);
}

/*
========================================
rtrim()
========================================
*/

std::string rtrim (
    const std::string&  str,
    const std::string&  trim_chars  /*= " \t"*/)
{
    const auto end = str.find_last_not_of(trim_chars);
    return (end == std::string::npos) ? "" : safe_substr(str, 0, end + 1);
}

/*
========================================
trim()
========================================
*/

std::string trim (
    const std::string&  str,
    const std::string&  trim_chars  /*= " \t"*/)
{
    return ltrim(rtrim(str, trim_chars), trim_chars);
}
