# Memphis 98
### MATE/GNOME icon theme intended to go with the GUI theme of the same name

## Workflow

My workflow is very manually-driven, and frankly kind of obtuse, so I'm open to suggestions. I'll try to describe my current structure and workflow:

<code>[_.redraws/](_.redraws/)</code> contains subdirectories, one for each icon that has been (or is being) redrawn. Most icons are used in multiple places, so the directory name for a particular redraw should be the most straightforward instance of a given icon's name. For example, "computer" is used for the following icons:<br>
<pre>
blueman-desktop               computer          computer-symbolic   gnome-dev-computer
gnome-dev-computer-symbolic   gnome-fs-client   system              gnome-computer
</pre>
</pre>

<hr>

<code>[\_.redraws/\_.resources/](_.redraws/_.resources/)</code> is basically just a junkpile for resources that I haven't found a place for yet&mdash;usually reference material (such as screenshots, icons, etc.) for icons that I have yet to begin re-drawing.

<hr>

<code>[actions/](actions/)</code>, <code>[animations/](animations/)</code>, <code>[apps/](apps/)</code>, <code>[categories/](categories/)</code>, etc., are category directories containing the original Classic95 set, minus any redraws. Once an icon has been redrawn, it is deleted from these directories; that is how I now what currently remains to be redrawn. <i>(Again, it's an obtuse system and I really should come up with something better.)</i>

<hr>

<code>[icon_copy_bin](icon_copy_bin)</code> is a binary (compiled C++) program that I use to merge my edits into my local theme directory (<code>~/.themes/Memphis-98</code>), as well as delete any original icons from the working directory, as mentioned above.

<code>[icon_copy](icon_copy)</code> is simply a tiny bash script that calls icon_copy_bin.

Upon running icon_copy_bin, the user is given three options:
1. Copy icon redraws (the process described above)
2. Generate the theme's index.theme file
3. Exit

The program uses the information in [_.redraws/redraw_targets.txt](_.redraws/redraw_targets.txt) to know what filenames to copy the new icons to, as well as which original icons to delete from the working directory. This file, unfortunately, has to be maintained by hand.

<hr>

<code>[_.icon_copy_src/](_.icon_copy_src/)</code> contains the C++ source code to icon_copy_bin.
