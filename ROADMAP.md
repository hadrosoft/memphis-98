# Memphis 98
# Roadmap

This list is broader than a TODO list, outlining general goals for the project rather than specific items.

- Continue to tweak GTK theme
    - Fix bugs
    - Make versions of theme for different Windows color schemes (Windows Classic, Windows Standard, Brick, Rose, Slate, etc.)
        - Will need to make metacity themes for each GTK theme

- Work on and finish icon theme
    - I have no experience with vector graphics, so currently, the icons are high-res raster graphics. I don't know if I should continue doing it this way. If anybody has experience making icons and wants to council me, or even wants to take on the icon project themselves, let me know! [Click here](icons/README.md) for details.

- Learn how to make mouse cursor themes; make themes to match Windows standard cursors and 3D Cursors (at the least)

- Windows 95 and Utopia sound schemes
    - Learn how to make sound themes

- Look into contributing to MATE project -- it would be cool to add CSS-selectable names to most widgets so they can be themed at will
    - Ideas for applications that could benefit from specific widget theming:
        - mate-calc would look awesome if specific groups of buttons could be different colors
